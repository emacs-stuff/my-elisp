;; I want an easy, interactive and out-of-the-box solution to write
;; blog posts in org-mode and publish them with the Nikola static site generator.
;; I want to either have all my posts in a single org file and publish a subtree,
;; either post an entire file.

;; requirements: org-mode plugin for Nikola: nikola plugin -i org-mode

;; How it works: The org plugin allows to write a post in the org
;; syntax. We simply have to insert the nikola headers at the top,
;; between an org comments block.

;; This file gives those handler functions:
;; - nikola-init: create a new project, asking for informations.
;; - nikola-set-post-properties: asks for all the needed post properties
;;  (title, date, description, tags).
;; - nikola-post-subtree: post the current subtree, asking for missing properties.

;; ideas: use helm and the tags plugin to select many existing tags.

;; Nikola wrapper (the static site generator)

(require 's)


(defvar nikola-default-site-title "My Nikola Site" "default site title")
(defvar nikola-default-site-author "Nikola Tesla")

(setq nikola-comment-systems (list "disqus" "foo"))

(defun nikola-init (dir title author email description url languages tz comment)
  "Get the basic information: title, author,... and create project."
  (interactive (list
                     (call-interactively 'nikola-choose-directory)
                     (read-from-minibuffer "Site title ? ")
                     (read-from-minibuffer "Site author ? ")
                     (read-from-minibuffer "Author's email ? ")
                     (read-from-minibuffer "Description: ")
                     (read-from-minibuffer "Site's URL: ")
                     (read-from-minibuffer "List of languages, comma separated: ")
                     (read-from-minibuffer "Correct timezone (Nikola uses the tw database): ")
                     (completing-read "Choose comment system: " nikola-comment-systems)
                     ))
  ;; TODO don't use cd-absolute
  (cd-absolute dir) ;; need to reset the original one.
  (cd-absolute "~/.emacs.d/my-elisp/")
  (nikola-init-quiet dir author title description)
)

(defun nikola--replace-author (dir author)
  "Replace the blog author in conf.py."
  (with-current-buffer (find-file-noselect (concat dir "/conf.py"))
  (beginning-of-buffer)
    (replace-regexp "^BLOG_AUTHOR = .*" (format "BLOG_AUTHOR = \"%s\" # (translatable)" author))
    (save-buffer)
))

(defun nikola--replace-title (dir title)
  ;; XXX: factorize with macros
  (with-current-buffer (find-file-noselect (concat dir "/conf.py"))
  (beginning-of-buffer)
    (replace-regexp "^BLOG_TITLE = .*" (format "BLOG_TITLE = \"%s\" # (translatable)" title))
    (save-buffer)
))

(defun nikola--replace-description (dir desc)
  ;; XXX: factorize with macros
  (with-current-buffer (find-file-noselect (concat dir "/conf.py"))
    (beginning-of-buffer)
    (replace-regexp "^BLOG_DESCRIPTION = .*" (format "BLOG_DESCRIPTION = \"%s\" # (translatable)" desc))
    (save-buffer)
))

;; exple to check if executable exists:
;;   (if (not (executable-find "fasd"))
;;               (error "Fasd executable cannot be found.  It is required by `fasd.el'.  Cannot use `fasd-find-file'")
;;             (unless query (setq query (if fasd-enable-initial-prompt
;;                                           (read-from-minibuffer "Fasd query: ")
;;                                         ""))

(defun nikola-init-quiet (dir author title description)
  ;;TODO: not finished !
  "Create the initial nikola project with the given information (blog author, etc)."
  (setq nikola--project-dir (cd-absolute "~/.emacs.d/my-elisp/")) ; projectile-project-root ?
  (setq retcode (call-process "nikola" nil (get-buffer-create "*nikola*") nil "init" "--quiet" dir))
  (if (not (equal retcode 0))
      (message "Nikola init failed. See *nikola* buffer.")
    (message "init ok. Now replace project info.")
    ;; TODO: finish
    (save-excursion
        (nikola--replace-author (concat nikola--project-dir "/" dir) author)
        (nikola--replace-title (concat nikola--project-dir "/" dir) title)
        (nikola--replace-description (concat nikola--project-dir "/" dir) description)
        )
    )
)

(defun nikola-choose-directory (dir)
  (interactive "DWhere to create the project ? ")
  dir
)

(defun current-line ()
  "returns the current line."
  ;; http://ergoemacs.org/emacs/elisp_all_about_lines.html
  (let (p1 p2 myLine)
    (setq p1 (line-beginning-position) )
    (setq p2 (line-end-position) )
    (setq myLine (buffer-substring-no-properties p1 p2))
    ))

(defun nikola-tags-get ()               ;TODO
  ;; thx http://punchagan.muse-amuse.in/tags/nikola.html
  "returns a list of tags used in this site. Uses nikola's tags plugin."
  (unless (file-exists-p "plugins/tags/")
    (error "Please install the 'tags' plugin with 'nikola plugin -i tags'."))
  (let* ((nikola-command (format "/home/%s/.virtualenvs/%s/bin/nikola"
                                 (getenv "USER") "site TODO") ; use venv-workon instead.
         (nikola-site (file-name-directory
                       (directory-file-name
                        (file-name-directory
                         (or (buffer-file-name (current-buffer)) "/")))))
         (tags (shell-command-to-string
                (format "cd %s && %s tags -l" nikola-site nikola-command))))
    (unless (search "ERROR" tags)
      (cdr (split-string tags "\n" t "\s+")))))
)

(defun nikola-tags-insert ()            ;TODO
  "Insert a nikola tag at point."
  (interactive)
  (let* ((word-match (or (current-word t) ""))
         (tags (completing-read-multiple "Tag: " (nikola-tags-get) nil nil word-match)))
    (when (and word-match tags)
      (delete-backward-char (length word-match)))
    (mapc (lambda (tag) (insert (format "%s, " tag))) tags))
)

(defun nikola-get-subtree-title ()
  "Return the title of the current subtree (what comes after the *)."
  (save-excursion
    (outline-previous-visible-heading 1)
    (s-trim (car (cdr (s-split "\*+" (current-line))))))
)

(defun nikola-get-slug (str)
  "rm unwanted characters from str."
  (let (slug)
    (setq slug str)
    ;; clean slug a little bit
    (setq slug (replace-regexp-in-string "[()]" "-" slug))
    (setq slug (s-trim slug))
    (setq slug (s-chop-suffix "-" slug))
    (setq slug (s-collapse-whitespace slug))
    (setq slug (downcase slug))
    (setq slug (s-replace " " "-" slug))
    (setq slug (s-replace "--" "-" slug))
    slug)
)

(defun nikola--read-title ()
  "Ask for the title with a suggestion."
  (read-from-minibuffer "title ? " (nikola-get-subtree-title))
)

(defun nikola--read-description ()
  "Ask for a description. Read the property if any."
  ;; I'm aware of org-read-property-value which does just that (and
  ;; better). My only concern is that the default value is given
  ;; between brackets and isn't editable.
  (read-from-minibuffer "description ? "
                        (org-entry-get nil "description"))
)

(defun nikola--read-tags ()
;; Multiple tag selection with completing-read-multiple.
;; Helm would be nice to mark the ones we want.
;; (completing-read-multiple "tags (comma separated) ? " (list "salut" "tag" "uie")) ;; works, but nikola tags -l is broken.
(read-from-minibuffer "tags (comma separated) ? "
                        (org-entry-get nil "tags"))
)

(defun nikola-set-post-properties (title date description tags)
  "Sets all post properties: title, tags, description, date. The
  slug is function of the title (spaces replaced by -)."
  (interactive (list (nikola--read-title)
                     (org-read-date)
                     (nikola--read-description)
                     (nikola--read-tags)
                     ))
  (org-set-property "title" title)
  (org-set-property "slug" (nikola-get-slug title))
  (org-set-property "date" date)
  (org-set-property "description" description)
  (org-set-property "tags" (mapconcat (lambda (tag) (s-trim tag)) ; what is the tags separator ?
                                      (s-split "," tags)
                                      ", "))
)

(defun nikola-post-subtree ()
    "copy the current tree to the post/ directory. We ask for a
     title and the other information needed by Nikola if they are
     not set as properties."
    (interactive)

    ;; Ask for missing properties: (to set with
    ;; nikola-set-post-properties) TODO: write the property if needed.
    ;; http://orgmode.org/manual/Using-the-property-API.html#Using-the-property-API
    (unless (setq title (org-entry-get (cdr (org-get-property-block)) "title"))
      (setq title (nikola--read-title)))

    (unless (setq description (org-entry-get (cdr (org-get-property-block)) "description"))
      (progn
        (setq description (nikola--read-description))
        (org-set-property "description" description)))

    (unless (setq tags (org-entry-get (cdr (org-get-property-block)) "tags"))
      (setq tags (nikola--read-tags)))
    (unless (setq slug (org-entry-get (cdr (org-get-property-block)) "slug"))
      (setq slug (s-replace " " "-" title)))
    (unless (setq date (org-entry-get (cdr (org-get-property-block)) "date"))
      (setq date (org-read-date)))

    (save-excursion
      (outline-previous-visible-heading 1)
      (setq cur-heading (car (s-split " " (current-line))))
      ; The beginning of the post is at the end of the properties' block.
      (let ((beg (progn (goto-char (cdr (org-get-property-block)))
                        (next-line)
                        (point)))
            (end (or (progn (if (search-forward-regexp "^\\*\\ " nil t) (point))) ;todo: search same level with cur-heading
                            ;; if there is no next subtree of same level:
                            (point-max)))
            )
        ; Copy the subtree. Not very org-ish I suppose.
        (setq curtext (buffer-substring-no-properties beg end))

        (with-temp-buffer
          (insert "#+BEGIN_COMMENT\n")
          (insert (format ".. title: %s \n" title))
          (insert (format ".. description: %s \n" description))
          (insert (format ".. tags: %s \n" tags))
          (insert (format ".. date: %s \n" date))
          (insert (format ".. slug: %s \n" slug))
          (insert "#+END_COMMENT\n")
          (insert curtext)
          (write-file (concat "posts/" slug ".org"))
          )
    )))

(defun nikola-auto ()
  "Run nikola auto: automatically detect site changes, rebuild and optionally refresh a browser.
  The nikola site must have a VCS root. Projectile needed."
  (interactive)
  (setq nikola-project-root (projectile-project-root))  ;; TODO: better
  (cd nikola-project-root)
  (let ((command (read-shell-command "Run command like this: " "nikola auto --port 8000")))
    (compile command)
    command
))

(defun nikola-auto-and-browse ()
  "Run nikola auto and open the default browser."
  (interactive)
  (let* ((command (nikola-auto))
         (port (if (s-contains? "-p" command)  ;; can evaluate before previous line and fail
                   (car (last (s-split " " command)))
                 "8000")))
    (browse-url (concat "http://localhost:" port "/index.html")))
  )

;; discover commands with a magit-like menu.
;; TODO: replace with a hydra.
;; requires https://github.com/mickeynp/discover.el

;; usage: s-n and choose action from the menu
;; TODO: allow to change keybinding.
;(require 'discover)

;; (discover-add-context-menu
;;  :context-menu '(nikola
;;               (description "Write static nikola sites with org-mode.")
;;               ;; (lisp-switches
;;                ;; ("-cf" "Case should fold search" case-fold-search t nil))
;;               ;; (lisp-arguments
;;                ;; ("=l" "context lines to show (occur)"
;;                 ;; "list-matching-lines-default-context-lines"
;;                 ;; (lambda (dummy) (interactive) (read-number "Number of context lines to show: "))))
;;               (actions
;;                ("nikola"
;;                 ("i" "init a new blog" nikola-init)
;;                 ("t" "set tags for this post" nikola-set-post-properties)
;;                 ("a" "nikola auto: build and auto refresh" nikola-auto)
;;                 ("p" "post subtree" nikola-post-subtree))
;; ))
;;  :bind "s-n")

;; (add-hook 'org-mode-hook 'discover--turn-on-nikola)
