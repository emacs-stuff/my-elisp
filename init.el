;; This sets up the load path so that we can override it
(package-initialize nil)
;; Override the packages with the git version of Org and other packages
(add-to-list 'load-path "~/elisp/org-mode/lisp")
(add-to-list 'load-path "~/elisp/org-mode/contrib/lisp")
;; Load the rest of the packages
;; (package-initialize t)
(setq package-enable-at-startup nil)
(require 'org)
(require 'ob-tangle)

(defvar emacs-configuration-directory
    "~/.emacs.legacy/"
    "The directory where the emacs configuration files are stored.
    Moved from .emacs.d to .emacs.legacy to use Doom in parallel.")

(defun fromconf (s)
  "Append .emacs.d (or .emacs.legacy) to this path (string).
   s does not start with a slash."
  (concat emacs-configuration-directory s))

(org-babel-load-file (expand-file-name (fromconf "my-elisp/myemacs.org")))

;; load .org only if it is newer than .el
;; see comment on http://endlessparentheses.com/init-org-Without-org-mode.html
;; (defvar init-source-org-file (expand-file-name "my-elisp/myemacs.org" user-emacs-directory)
;;   "The file that our emacs initialization comes form")

;; (defvar init-source-el-file (expand-file-name "my-elisp/myemacs.el" user-emacs-directory)
;;   "The file that our emacs initialization is generated into")

;; (if (file-exists-p init-source-org-file)
;;   (if (and (file-exists-p init-source-el-file)
;;            (time-less-p (nth 5 (file-attributes init-source-org-file)) (nth 5 (file-attributes init-source-el-file))))
;;       (load-file init-source-el-file)
;;     (if (fboundp 'org-babel-load-file) ; ' disqus syntax highlighting is lame
;;         (org-babel-load-file init-source-org-file)
;;       (message "Function not found: org-babel-load-file")
;;       (load-file init-source-el-file)))
;;   (error "Init org file '%s' missing." init-source-org-file))


;; load elisp without loading org
;; http://endlessparentheses.com/init-org-Without-org-mode.html
;; (defvar endless/init.org-message-depth 3
;;   "What depth of init.org headers to message at startup.")

;; ;; caveat: it doesn't recognize the :tangle no
;; (with-temp-buffer
;;   (insert-file "~/.emacs.d/my-elisp/myemacs.org")
;;   (goto-char (point-min))
;;   (search-forward "End:") ;; search for beginning of text to eval
;;   (while (not (eobp))
;;     (forward-line 1)
;;     (cond
;;      ;; Report Headers
;;      ((looking-at
;;        (format "\\*\\{2,%s\\} +.*$"
;;                endless/init.org-message-depth))
;;       (message "%s" (match-string 0)))
;;      ;; Evaluate Code Blocks
;;      ((looking-at "^#\\+BEGIN_SRC +emacs-lisp *$")
;;       (let ((l (match-end 0)))
;;         (search-forward "\n#+END_SRC")
;;         (eval-region l (match-beginning 0))))
;;      ;; Finish on the next level-1 header
;;      ((looking-at "^\\* ")
;;       (goto-char (point-max))))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ag-arguments (quote ("--smart-case" "--stats")))
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" . t)))
 '(my-indent-tool-arguments nil)
 '(safe-local-variable-values
   (quote
    ((Base . 10)
     (Syntax . ANSI-Common-Lisp)
     (py-isort-options)
     (py-indent-offset . 2)
     (py-smart-indentation)
     (py-continuation-offset . 4)
     (flycheck-pylintrc . "pylintrc")
     (python-smart-indentation)
     (python-continuation-offset . 4)
     (gitlab-host . https://framagit\.org)
     (gitlab-username . "ehvince")))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'narrow-to-region 'disabled nil)
