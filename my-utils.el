(require 's)

(defun load-this-buffer ()
  "Load current buffer. Handy in development. Bind to a key. "
         (interactive)
         (load (buffer-file-name (current-buffer))))

(defun my-load-all-in-directory (dir)
  ;; http://stackoverflow.com/questions/18706250/emacs-require-all-files-in-a-directory
  "`load' all elisp libraries in directory DIR which are not already loaded."
  (interactive "D")
  (let ((libraries-loaded (mapcar #'file-name-sans-extension
                                  (delq nil (mapcar #'car load-history)))))
    (dolist (file (directory-files dir t ".+\\.elc?$"))
      (let ((library (file-name-sans-extension file)))
        (unless (member library libraries-loaded)
          (load library nil t)
          (push library libraries-loaded))))))

(defun unquote ()
  "delete surrounding double quotes (need to be in between)"
         (interactive)
         (search-backward "\"")
         (delete-char 1)
         (search-forward "\"")
         (delete-char -1)
)
(defun untick ()
  "delete surrounding quotes (need to be in between)"
         (interactive)
         (search-backward "'")
         (delete-char 1)
         (search-forward "'")
         (delete-char -1)
)
(defalias 'untick 'unquote)
(defalias 'untick 'my-untick)

;; to see: http://ergoemacs.org/emacs/elisp_change_brackets.html
(defun quote2double ()
  "transform surrounding single quotes to doubles (need to be in between)"
  (interactive)
  (search-backward "'")
  (delete-char 1)
  (insert "\"")
  (search-forward "'")
  (delete-char -1)
  (insert "\""))

(defun double2quote ()
  "transform surrounding double quotes to single ones (need to be in between)"
  (interactive)
  (search-backward "\"")
  (delete-char 1)
  (insert "'")
  (search-forward "\"")
  (delete-char -1)
  (insert "'"))

;; oct 13
(defun list2dict ()
  (interactive)
  (search-backward "[")
  (delete-char 1)
  (insert "{")
  (search-forward "]")
  (delete-char -1)
  (insert "}")
)

(defun list2parenthesis ()
  (interactive)
  (search-backward "[")
  (delete-char 1)
  (insert "(")
  (search-forward "]")
  (delete-char -1)
  (insert ")")
)

(defun dict2list ()
  (interactive)
  (search-backward "{")
  (delete-char 1)
  (insert "[")
  (search-forward "}")
  (delete-char -1)
  (insert "]")
)

(defun tuple2list ()
  "not so dummy implementation for it uses forward-sexp to go to
   the balanced parenthesis."
  (interactive)
  (search-backward "(")
  (delete-char 1)
  (insert "[")
  (backward-char)
  (forward-sexp )
  (backward-char)
  (delete-char 1)
  (insert "]")
  )

(defvar editdoc-doc-quotes "\"\"\""
  "the string which quotes a docstring.")

(defun current-line ()
  "returns the current line."
  ;; http://ergoemacs.org/emacs/elisp_all_about_lines.html
         (let (p1 p2 myLine)
           (setq p1 (line-beginning-position) )
           (setq p2 (line-end-position) )
           (setq myLine (buffer-substring-no-properties p1 p2))
           ))

(defun current-line-contains? (needle)
  "returns t if current line contains the given string."
  (s-contains? needle (current-line))
)

(defun current-line-indentation ()
  "returns the str of the current indentation (spaces)."
  ;; https://github.com/magnars/s.el#s-match-strings-all-regex-string
  (car (car (s-match-strings-all "^\s+" (current-line)) ) )
  )

(defun my-org-link-to-rst ()
         "[[http:url][the title ]] => `the title <http:url>`_"
         (interactive)
         (replace-regexp "\\[\\[\\(http.*\\)\\]\\[\\(.*\\)\\]\\]" "`\\2 <\\1>`_")
         )

(defvar editdoc-doc-quotes "\"\"\""
  "the string which quotes a docstring.")

(defun my-editdoc-goto-insert ()
  "add documentation of a defun based on its arguments (and primarly based on python's triple quotes, but to set with editdoc-doc-quotes."
  (interactive)
  (save-excursion
    (beginning-of-defun)
    (next-line) (beginning-of-line)
    (setq triple-quotes "\"\"\"")
    (setq mycurindent (current-line-indentation))
    (if (current-line-contains? triple-quotes)
        (progn (search-forward triple-quotes)
               (search-forward triple-quotes)
               (backward-char) (backward-char) (backward-char)
               (insert (concat "\n" mycurindent "continuing the doc !"))
               (message "contains triple quotes")
               )
      (progn
        (insert (concat mycurindent "\"\"\"some doc !" "\n" mycurindent "\"\"\"\n"))
        )))
)

(defun current-line-indentation ()
  "returns the str of the current indentation (spaces)."
  ;; https://github.com/magnars/s.el#s-match-strings-all-regex-string
  (car (car (s-match-strings-all "^\s+" (current-line)) ) )
  )

(defun my-import-line-get-file ()
  "on a python import statement, open the associated file (if in project)."
  (let ((file-ext (file-name-extension (buffer-file-name))))
    ;; see also projectile-project-root
    (concat (magit-get-top-dir)  ;; doesn't work with imports from other project…
            (s-replace "." "/" (car (cdr (s-split " "(current-line)))))
            "."
            file-ext)
  ))

(defun my-py-import-line-goto-module ()
  "must be on a line like
   from foo import Bar
   will open file foo and search for [def|class] Bar"
  (interactive)
    (progn
      (let ((tosearch (car (last (s-split " " (current-line))) )))  ;; module name, last of import line
            (find-file-other-window (my-import-line-get-file))
            (search-forward (concat "class " tosearch))
            (recenter "top")
            )
      )
    )

(defun my-string-matching (regexp str match)
  "return the string matching the regexp."
  ;; s-match-strings-all is similar.
  (string-match regexp str)
  (substring str (match-beginning match) (match-end match))
)

(defun my-py-get-function-args ()
  "get a list of the method's arguments. They must be separated
  by a comma followed by a space (this is dumb but the solidity is
  considered satisfactory). "
  (interactive)
  (save-excursion
    (save-restriction
      (beginning-of-defun)
      (setq myfoo-str (my-string-matching  "(\\(.*\\))" (current-line) 1))
      (s-split ", " myfoo-str)
      ;; (message myfoo-str)
)))

(defun my-py-add-arg (arg)
  "add an argument to a python function. This is done better by Rope."
  ;; in a big python base (1000+ py files, with setups, unit tests, configs, mixins):
  ;; - some signatures with a void list: arg=foo[]
  ;; - one arg with open parentheses + equal sign: arg='(object=foo)' which doesn't contain commas.
  ;; - not a comma not followed by a space
  ;; So split the arguments string by ", " ?
  (interactive "sArgument? ")
  (save-excursion
    (save-restriction
    ;; if not on line containing "^def [space]", go to beginnig of defun.
    ;; (search-backward "):")
    (beginning-of-defun) (search-forward "):") (backward-char 2)
    (if (looking-back "*args") ;;TODO: use my-py-get-function-args
        (progn
          (backward-char 5)
          (insert ", ")
          (backward-char 2))
      ;; else
      (if (looking-back "**kwargs")
          (progn
            (backward-char 8)
            (if (looking-back "*args, ")
                (backward-char 9))
            (insert ", ")
            ))
      (insert ", "))
    (insert arg)
    (if (looking-at "\\*\\*") (insert ", "))
    ;; should offer to re-order the args (if we have **kwargs)
    (save-buffer)
  )))

(defun sphinx-format ()
  "If the current defun has no docstring, add a sphinx-style one,
  asking interactively for types and doc about all the function
  arguments."
  ;; weaknesses: indentation
  ;; idea: - remember already typed, parse current doc.
  (interactive)
  (save-excursion
    (beginning-of-defun)
    (next-line)
    (if (looking-at " *\"\"\"")
        (progn
          (message "we have a docstring")
          (next-line)
          (if (looking-at ".*:param")
              (message "and we have a :param"))
          )
      (insert "    \"\"\"\n")
      (mapcar (lambda (arg)
                (setq cur-type (read-from-minibuffer (format "type of %s ? " arg)))
                (setq cur-doc (read-from-minibuffer (format "doc of %s ? " arg)))
                (insert (format "    :param %s %s: %s\n" cur-type arg cur-doc))
                )
              (my-py-get-function-args))
        (insert "    \"\"\"\n"))
    ))

(defun print2logging-line ()
  "transforms a print to a logging.info."
  (interactive)
  (save-excursion
    (let ((beg (progn (beginning-of-line) (point)))
          (end (progn (end-of-line) (point))))
      (replace-regexp "print \\(.*\\)" "logging.info(\\1)" nil beg end)
      )))

(defun print2logging-buffer (log level)
  "transforms print statements to a log.level, interactively."
  ;; doesn't work with multi-line prints and ;-separated prints.
  (interactive "sLog statement ? \nslevel ? ")
  (save-excursion
    (query-replace-regexp "print \\(.*\\)" (format "%s.%s(\\1)" log level) nil (point-min) (point-max))
    )
  )

(defun my-elisp-add-arg (arg)
  "add an argument to an elisp function."
  (interactive "sArg? ")
  (save-excursion
    (search-backward "(defun")
    (search-forward ")") (backward-char)
    (insert (if (looking-back "(")
                arg
              (concat " " arg)))
  ))

;; Django
(defun my-django-add-to-installed-apps (app)
  "Add an app to the settings' installed_app.
   Very basic, based on search of this string. Adds the app in
   the end of the list."
         (interactive "sWhat app to add ? ")
         (find-file (concat (projectile-project-root) "abelujo/" "settings.py"))
         (save-excursion
         (beginning-of-buffer)
         (search-forward "INSTALLED_APPS = (")
         (search-forward ")")
         (previous-line)
         (newline-and-indent)
         (insert (concat "'" app "',")) (newline-and-indent)
         ))

(defun my-django-trans-region (beg end)
  "Add translate tags around the region."
  (interactive "r")
  (save-excursion
    (let* ((left "{% trans \"")
           (right "\" %}")
           (end (+ end (length left))))
      (goto-char beg)
      (insert left)
      (goto-char end)
      (insert right))))

(defun my-django-trans-word-or-region ()
  "add translation tags to the word we're on."
  (interactive)
  (if (region-active-p)
      (call-interactively 'my-django-trans-region)
  (save-excursion
    (backward-word)
    (insert "{% trans \"")
    (forward-word)
    (insert "\" %}"))))

(defun my-django-ng-region (beg end)
  "Add '...'| ng around region"
  (interactive "r")
  (save-excursion
    (goto-char beg)
    (insert "'")
    (goto-char end) (forward-char)
    (insert "'|ng ")))

(defun my-django-ng-word-or-region ()
  "add '...'|ng around word or region, for javascript."
  (interactive)
  (if (region-active-p)
      (call-interactively 'my-django-ng-region)
    (save-excursion
      (backward-word)
      (insert "'")
      (forward-word)
      (insert "'|ng "))))

(defhydra my-django-hydra (:color blue :columns 2)
  "Django util functions"
  ("|" (my-django-ng-word-or-region) "escape for javascript with 'ng'")
  ("t" (my-django-trans-word-or-region) "add translation around word or region")
  )


;; TODO factorize between the 2.
(defun my-python-add-requirement (req doc)
  "Add an entry to the requirements file."
  ;; TODO: version nb. This would be much better with a pip option.
         (interactive "sWhat package to add ? \nsOptional comment: ")
         (setq requirements-filename "requirements.txt")
         (find-file (concat (projectile-project-root) "abelujo/" requirements-filename))
         (save-excursion
           (end-of-buffer)
           (insert (concat req
                           (unless (string= "" doc) (concat " # " doc))))
           )
         )


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; org-mode
;; Enclose the region in src blocks.
(defun my-region-to-block ()
  "in an org file, divide a code block in two parts."
  (interactive)
  (if (not (use-region-p))
      (error "no region is highlighted")

    (let ((text (buffer-substring (region-beginning) (region-end))))
      (delete-region (region-beginning) (region-end)) ; why delete ?! TODO:
      (insert (format "#+BEGIN_SRC emacs-lisp\n%s\n#+END_SRC\n" text ))))
)

;; replace latin-1 accents. Needs a refacto, yes.
(defun my-accents-replace ()
         "replace latin accents to utf-8. Check the file encoding system is utf-8 with M-x set-buffer-file-encoding-system."
         (interactive)
         (setq set-buffer-file-coding-system "utf-8")
         (beginning-of-buffer) (replace-string "�" "À")
         (beginning-of-buffer) (replace-string "�" "É")
         (beginning-of-buffer) (replace-string "�" "à")
         (beginning-of-buffer) (replace-string "�" "â")
         (beginning-of-buffer) (replace-string "�" "é")
         (beginning-of-buffer) (replace-string "�" "è")
         (beginning-of-buffer) (replace-string "�" "ê")
         (beginning-of-buffer) (replace-string "�" "î")
         (beginning-of-buffer) (replace-string "�" "ç")
         (beginning-of-buffer) (replace-string "�" "ë")
         (beginning-of-buffer) (replace-string "�" "ô")
         (beginning-of-buffer) (replace-string "�" "ù")
         (beginning-of-buffer) (replace-string "�" "û")
         )


(defun buffer-match-all (regexp filename)
  "return the list of matching results.
   It doesn't count group matches, but could.
   TODO: give regexp and filename as arguments."
  ;; https://stackoverflow.com/questions/1642184/extracting-urls-from-an-emacs-buffer
  ;; group matches: http://www.emacswiki.org/emacs/ElispCookbook#toc36
  (interactive)
  (save-restriction
    (setq myresult '())
    (goto-char 1)
    ;; algo is: search forward without stopping (3rd arg is t)
    ;; and at each found occurence, append the matched string to the result.
    (let ((case-fold-search nil))
      (setq myresult '())
      (while (search-forward-regexp "^[a-z]+" nil t)
        (progn
          (if (null myresult) (setq myresult (list (match-string 0))))
          (setq myresult (cons (match-string 0) myresult))

        ))
      myresult)))

(defun makefile-get-commands ()
  "TODO: finish. see how to handle path to files etc."
  (file-name-as-directory)
  (buffer-match-all ("^[a-z0-9]+" "Makefile")) ;needs absolute filename ?
)

(defvar git-clone-default-directory (expand-file-name "~/bacasable/"))

(setq git-clone-recent-repo "")

(defun git-clone-get-default-directory ()
  (if (file-exists-p (expand-file-name git-clone-default-directory))
    git-clone-default-directory
    "nil")
)

(defun git-clone-clean-dir-name (dir)
  (s-chop-suffixes '(".git") dir)
)

(defun git-clone (url directory)
  "Ask a git repo to clone and the directory (default is git-clone-default-directory)."
  (interactive (list (read-from-minibuffer "url?")
                     (read-directory-name "Directory ? " (git-clone-get-default-directory))))
  (setq git-clone-cur-directory directory)
  (if (s-blank? directory) (setq git-clone-cur-directory git-clone-default-directory))
  (setq git-clone-recent-repo (git-clone-clean-dir-name (-last-item (s-split "/" url))))
  ;; (compile (format "cd %s && git clone %s" directory url))
  (shell-command (format "cd %s && git clone %s" git-clone-cur-directory url) (get-buffer-create "*Messages*"))
  )

(defun git-clone-and-open ()
  (interactive)
  (call-interactively 'git-clone)
  ;; TODO: if recent repo ends in .git, rm it.
  (find-file (concat git-clone-cur-directory git-clone-recent-repo))
)

(defun my-rst-link (txt link)
         (interactive "stitle?\nslink? ")
         (insert (concat "`" txt " < " link " >`_")))
(defalias 'rst-link 'my-rst-link)

;; https://github.com/magnars/.emacs.d/blob/master/defuns/misc-defuns.el
;; start a httpd-server in current directory
(defun httpd-start-here (directory port)
  (interactive (list (read-directory-name "Root directory: " default-directory nil t)
                     (read-number "Port: " 8017)))
  (setq httpd-root directory)
  (setq httpd-port port)
  (httpd-start)
  (browse-url (concat "http://localhost:" (number-to-string port) "/")))

(provide 'my-utils)
