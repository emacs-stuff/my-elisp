(require 'my-utils) ;; requires current-line-indentation from my-utils.

;;;; USELESS.
;;;; see https://gitlab.com/emacs-stuff/indent-tools
;;;; and other shiny yaml packages.

(defvar my-yaml-element-regexp "\"?[a-zA-Z0-9\.]+" "regex to recognize a line.") ;; everything not space or comment

(defun current-line ()
  "returns the current line."
  ;; http://ergoemacs.org/emacs/elisp_all_about_lines.html
         (let ( (p1 (line-beginning-position))
                (p2 (line-end-position)))
           (buffer-substring-no-properties p1 p2)
           ))

(defun current-line-contains? (needle)
  "returns t if current line contains the given string."
  (s-contains? needle (current-line))
)

(defun current-line-indentation ()
  "returns the str of the current indentation (spaces)."
  ;; https://github.com/magnars/s.el#s-match-strings-all-regex-string
  (car (car (s-match-strings-all "^\s+" (current-line)) ) )
  )

;; Yaml to hashmap

(defun yaml-parse ()
  "yaml to json to a hashmap of current buffer, with python.

   There is no yaml parser in elisp.
   You need pyYaml and some yaml datatypes like dates are not supported by json."
  (interactive)
  (let ((json-object-type 'hash-table))
    (setq  myyaml (json-read-from-string (shell-command-to-string (concat "python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout, indent=4)' < " (buffer-file-name))))))
  ;; code here
  )

(defun hash-all-keys (hash)
  "Return all keys of  hashmap. In emacs 24.4, use hash-table-keys."
  ;; http://ergoemacs.org/emacs/elisp_hash_table.html
  (let (allkeys) (maphash (lambda (k v) (setq allkeys (cons k allkeys))) hash) allkeys)
)

;; Get all defaults of a query: (specific code)
(defun my-yaml-all-defaults (query myyaml)
  "all keys of query.default"
  (hash-all-keys (gethash "defaults" (gethash query myyaml)))
)

(defun my-yaml-get-default (elt query myyaml) ; specific code
  "get the key named 'default'."
   (gethash elt (gethash "defaults" (gethash query myyaml)) "")
   )

;; replace a given elt by its default in the query
(defun my-yaml-replace-elt-in-query (elt query myyaml)
  (s-replace (format "{%s}" elt) (number-to-string (my-yaml-get-default elt query myyaml)) (gethash "query" (gethash query myyaml)))
)

(defun my-yaml-loop ()
  "for each value in defaults, replace its occurence in the query by its value."
(setq curstr "{months}")
(loop for elt in (my-yaml-all-defaults "patches" myyaml)
      do (setq curstr (s-replace (format "{%s}" elt)
                                 (progn (setq _cur (my-yaml-get-default elt "patches" myyaml))
                                        (if (numberp _cur) (number-to-string _cur) _cur))
                                 curstr ) ))
curstr
)

;; Yaml navigation:

(defun my-yaml-next-sibling ()
  "Goes to the next element of the same level (defined by the current line indentation)."
  (interactive)
  (end-of-line)
  (setq my-yaml-element-regexp "\"?[a-zA-Z0-9]")
  ;; (setq my-yaml-element-regexp ".*") ;; should not start by a comment
  (or (search-forward-regexp (concat "^"
                                 (current-line-indentation)
                                 my-yaml-element-regexp)
                         nil ; don't bound the search
                         t ; if search fails just return nil, no error
                         )
      (goto-char (point-max)))
  (beginning-of-line-text))

(defun my-yaml-previous-sibling ()
  "Go to previous sibling."
  (interactive)
  ;; (beginning-of-line-text)
  (beginning-of-line)
  (search-backward-regexp (concat "^"
                                  (current-line-indentation)
                                  my-yaml-element-regexp))
  (beginning-of-line-text))

(defun yaml-siblings ()
  "Choose a sibling of current node to go to."
  (interactive)
  ;; Can we have some fucking developper documentation please ?
  (call-interactively 'helm-swoop (concat "^" (current-line-indentation) my-yaml-element-regexp))
)

(defhydra yaml-utils-hydra (:color red :columns 2)
  "Indentation-based utils"
  ("n" (my-yaml-next-sibling) "next sibling")
  ("p" (my-yaml-previous-sibling) "previous sibling")
  )
