;; base tool:
;; npm info package-foo repository.url and stuff: see npm help show
;; to remember: npm can generate a json output with --json

(defmacro npm-interactive-default () '
  (read-from-minibuffer (concat "Package ? (default: "
                                (setq cur-word (thing-at-point 'word))
                                "): "))
)

(defun npm-view (package &optional args)
  "call: npm view <package> <args>"
  (shell-command-to-string (concat "npm view " package " " args))
)

(defun npm-homepage (package)
  "Open package's homepage in browser. Suggests the word at point."
  (interactive (list (read-from-minibuffer (concat "Package ? (default: "
                                                   (setq cur-word (thing-at-point 'word))
                                                   "): "))))
  (if (s-blank? package)
    (setq package cur-word))
  (browse-url (shell-command-to-string (concat "npm info " package " homepage")))
  ;; todo: error handling
  )

(defun npm-description (package)
  "print package's description."
  (interactive (list (npm-interactive-default)))
  (if (s-blank? package) (setq package cur-word))
  (message (npm-view package "description"))
)

(defun npm-install (package)
  "Install globally an npm package."
  (interactive "sPackage ? ")
  (compile (format "npm install -u %s" package))
)
