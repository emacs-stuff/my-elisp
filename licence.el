;; Add a licence header to all files of a project.
;; vindarel 2014 copyleft

;; Package-Requires: (projectile, dash.el, s.el)

;; Put the text you want to insert in a file called licence-header.txt at the root of your project.
;; If this file doesn't exist, you'll be asked for one.
;; See the filters in the function licence-get-project-src-files.
;; We open all the files and don't close any, sorry !

;; Requisite: start the command with a two-sided window...

(defun licence-get-project-src-files ()
  (interactive)
  (setq files (shell-command-to-string "git ls-files"))
  (setq files (s-split "\n" files))
  ;; TODO: use git ls-files --exclude=<pattern>
  (setq files  (--filter (not (s-starts-with? "static/" it)) files))
  (setq files  (--filter (not (s-starts-with? "doc/" it)) files))
  (setq files  (--filter (not (s-starts-with? "COPYRIGHT" it)) files))
  (setq files  (--filter (not (s-starts-with? "LICENCE" it)) files))
  (setq files  (--filter (not (s-starts-with? "Makefile" it)) files))
  (setq files  (--filter (not (s-ends-with? ".js" it)) files))
  (setq files  (--filter (not (s-ends-with? ".css" it)) files))
  (setq files  (--filter (not (s-ends-with? ".map" it)) files))
  (setq files  (--filter (not (s-ends-with? ".csv" it)) files))
  (setq files  (--filter (not (s-ends-with? ".json" it)) files))
  (setq files  (--filter (not (s-ends-with? ".md" it)) files))
  (setq files  (--filter (not (s-ends-with? ".rst" it)) files))
  ;; (setq files  (--filter (not (s-ends-with? "__init__.py" it)) files)) ; temp
  (setq files (--map (concat (projectile-project-root) it) files))
  ;; (message (mapconcat (lambda (str) str) files " "))
  )

(defun licence--ask-licence-name (file)
  (interactive "fLicence's header file ? ")
  file
)

(defun licence--get-licence-header ()
  "If file licence-header.txt at project root doesn't exist, ask for one."
  (setq licence-header (concat (projectile-project-root) "licence-header.txt"))
  (unless (file-exists-p licence-header)
    (setq licence-header (call-interactively 'licence--ask-licence-name)))
  licence-header
)

(defun file-lines-nb (file)
  "Return the number of lines in this file."
  (safe-length (s-split "\n" (with-temp-buffer
                               (insert-file-contents file)
                               (buffer-string))))
)

(defun licence-add-licence ()
  "Add a commented licence header to all files of your project (see the filters).
Warning: we rely on find-file-other-window, so you must have a
two-sided window configuration before starting."
  ;; We need to open every file to use the right comment syntax.
  (interactive)
  (setq files (licence-get-project-src-files))
  (setq licence-header (licence--get-licence-header))
  (setq licence-size (file-lines-nb licence-header))
  (setq licence-size (- licence-size 1))             ; warning. May be my own pb of my comment system.
  (loop for file in files
  ;; (loop for file in (-take 7 files)  ;; testing purposes
        do
        (setq buf (find-file-other-window file))
        (goto-char (point-min))
        (if (looking-at "#!")
            (next-line))
        (if (looking-at "# -*-")        ; only for py files TODO:
            (next-line))
        (insert-file-contents licence-header)
        (comment-region (point) (progn (next-line licence-size) (point)))
        (save-buffer)
        (other-window 1))
  )

;; licence.el ends here.
